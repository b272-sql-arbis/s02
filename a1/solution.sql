CREATE DATABASE enrollment_db;

CREATE TABLE students (
	student_id INT NOT NULL AUTO_INCREMENT,
	student_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (student_id)
);

CREATE TABLE teachers (
	teacher_id INT NOT NULL AUTO_INCREMENT,
	teacher_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (teacher_id)
);

CREATE TABLE courses (
	course_id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(50) NOT NULL,
	teacher_id INT NOT NULL,
	PRIMARY KEY (course_id),
	CONSTRAINT fk_courses_teachers_id
	FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);

CREATE TABLE student_courses (
	course_id INT NOT NULL,
	student_id INT NOT NULL,
	CONSTRAINT fk_student_courses_courses_id
	FOREIGN KEY (course_id) REFERENCES courses(course_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
	CONSTRAINT fk_student_courses_students_id
	FOREIGN KEY (student_id) REFERENCES students(student_id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);